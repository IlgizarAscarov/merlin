<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('barcode','ProductController@barcode');
Route::get('products','ProductController@search');
Route::get('ymarket','ProductController@ymarket');

Route::get('addcall','CallManagerController@addcall');
Route::get('checkcall','CallManagerController@checkcall');
Route::get('statuscall','CallManagerController@statuscall');
