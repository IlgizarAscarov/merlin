<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('category',50);
          $table->string('barcode',50);
          $table->string('barcodeMerlin',50);
          $table->string('photo',50);
          $table->float('price',10,2);
          $table->string('position',100);
          $table->text('instruction');
          $table->text('recycling');
          $table->string('keywords',100);
          $table->bigInteger('yandexId');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
