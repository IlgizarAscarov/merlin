<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CallManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('callManager', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user');
          $table->integer('consultant')->nullable();;
          $table->integer('status');
          $table->time('timerSearch')->nullable();
          $table->time('timeHelp')->nullable();
          $table->string('position',100);

          //$table->string('upto',100);

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
