<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CallManager;
use App\Product;
use DateTime;

class CallManagerController extends Controller
{
  public const status = array(
    1=>"Новая",
    2=>"Ищу",
    3=>"Помогаю",
    4=>"Помог",
    5=>"Отменен"
);

    public function addcall(Request $request){
      $id = $request->input('productId');
      $userId = $request->input('userId');
      if(!$userId) $userId = 1;

      $product = Product::where('id','=',$id)->first();

      $call = new CallManager;

      $call->status = 1;
      $call->position = $product->position;
      $call->user = $userId;


      $call->save();

      return response()->json(['status'=>'ok'],200);
    }

    public function checkcall(Request $request){
      //$consultantId = $request->input('consultantId');
      $userId = $request->input('userId');

      if($userId){
        $call = CallManager::where('user','=',$userId)
          ->orderBy('id', 'desc')
          ->first();

        return response()->json(['status'=>'ok','callList'=>$call],200);
      }else{
        $callList = CallManager::where('status','=',1)
          ->orderBy('id', 'asc')->get();

        return response()->json(['status'=>'ok','callList'=>$callList],200);
      }
    }




    public function statuscall(Request $request){
      $id = $request->input('id');
      $status = $request->input('status');
      $consultantId = $request->input('consultantId');

      $call = CallManager::where('id','=',$id)->first();

      $call->status = $status;
      if($consultantId) $call->consultant = $consultantId;
      $now = new DateTime();

      if($status==3){
        $create = $call->created_at;
        $interval = $create->diff($now);
        $timerSearch = $interval->format('%h:%i:%s');
        $call->timerSearch = $timerSearch;
      }
      if($status==4){
        $create = $call->updated_at;
        $interval = $create->diff($now);
        $timerHelp = $interval->format('%h:%i:%s');
        $call->timeHelp = $timerHelp;
      }
      $call->save();

      return response()->json(['status'=>'ok','call'=>$call],200);
    }

}
